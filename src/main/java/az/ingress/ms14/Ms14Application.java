package az.ingress.ms14;

import az.ingress.ms14.model.*;
import az.ingress.ms14.repository.AddressRepository;
import az.ingress.ms14.repository.BranchRepository;
import az.ingress.ms14.repository.MarketRepository;
import az.ingress.ms14.repository.RegionRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class Ms14Application implements CommandLineRunner {


    private final MarketRepository marketRepository;
    private final BranchRepository branchRepository;
    private final AddressRepository addressRepository;
    private final RegionRepository regionRepository;
     private  final EntityManagerFactory entityManagerFactory;




    public static void main(String[] args) {

        SpringApplication.run(Ms14Application.class, args);
    }


    @Override
    @Transactional
    public void run(String... args) throws Exception {

//
//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        entityManager.getTransaction().begin();
//
//        List <Branch>  list = entityManager.createNativeQuery("select * from branch where market_id = :id", Branch.class)
//                .setParameter("id", 1).getResultList();
//        list.forEach(System.out::println);


//        Market bravo = marketRepository.findById(1L).get();
//        Market araz = marketRepository.findById(3L).get();
//
//
//        Region absheron = Region.builder()
//                .name(RegionType.ABSHERON)
//                .build();
//
//        Region merkez = Region.builder()
//                .name(RegionType.MERKEZ)
//                .build();
//        regionRepository.save(absheron);
//        regionRepository.save(merkez);
//
//        bravo.getRegions().add(absheron);
//        bravo.getRegions().add(merkez);
//        araz.getRegions().add(absheron);
//        araz.getRegions().add(merkez);
//
//        marketRepository.save(bravo);
//        marketRepository.save(araz);
//        Market market = Market.builder()
//                .name("Araz")
//                .type("SuperMarket")
//                .build();
//
//        Branch korogluBranch = Branch.builder()
//                .name("azadliq")
//                .countOfEmployee(100)
//                .market(market)
//                .build();
//        Branch ahmadliBranch=Branch.builder()
//                .name("Bineqedi")
//                .countOfEmployee(150)
//                .market(market)
//                .build();
//        market.getBranches().add(korogluBranch);
//        market.getBranches().add(ahmadliBranch);
//
//        Address address1 = Address.builder()
//                .name("azadliq m/s, 55")
//                .build();
//
//        Address address2 = Address.builder()
//                .name("bineqedi sck,333")
//                .build();
//
//        korogluBranch.setAddress(address1);
//        ahmadliBranch.setAddress(address2);
//
//
//            marketRepository.save(market);
//        marketRepository.findAll().forEach(System.out::println);


//        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5455/postgres",
//                "postgres", "password");
//
//        Statement statement = connection.createStatement();
//        ResultSet resultSet = statement.executeQuery("select * from student ");
//        while (resultSet.next()){
//            System.out.println(resultSet.getInt(1));
//            System.out.println(resultSet.getInt(2));
//            System.out.println(resultSet.getString(3));
//        }

//        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnitName");
//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        entityManager.getTransaction().begin();
//
//        List resultList = entityManager.createNativeQuery("select * from student ", Student.class).getResultList();
//        System.out.println(resultList);
//        entityManager.getTransaction().commit();
//        entityManager.close();


    }
}
