package az.ingress.ms14.service;

import az.ingress.ms14.dto.RegionRequest;
import az.ingress.ms14.dto.RegionResponse;
import az.ingress.ms14.model.Region;
import az.ingress.ms14.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegionService {

    private final ModelMapper modelMapper;
    private final RegionRepository regionRepository;

    public RegionResponse create(RegionRequest request) {

        Region region = modelMapper.map(request, Region.class);
        regionRepository.save(region);

        return modelMapper.map(region,RegionResponse.class);



    }

    public void update(Long regionId, RegionRequest request) {

        Region region = regionRepository.findById(regionId).
                orElseThrow(() -> new RuntimeException(String.format("region with id %s not found", regionId)));

        region.setName(request.getName());

        regionRepository.save(region);


    }

    public RegionResponse get(Long id){

        Region region = regionRepository.findById(id).
                orElseThrow(() -> new RuntimeException(String.format("region with id %s not found", id)));

        return modelMapper.map(region,RegionResponse.class);
    }

    public void delete(Long id){
        regionRepository.deleteById(id);
    }
}
