package az.ingress.ms14.service;

import az.ingress.ms14.dto.AddressRequest;
import az.ingress.ms14.dto.AddressResponse;
import az.ingress.ms14.model.Address;
import az.ingress.ms14.model.Branch;
import az.ingress.ms14.repository.AddressRepository;
import az.ingress.ms14.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;
    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;


    public AddressResponse create(Long branchId, AddressRequest request) {

        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Address with id %s not found", branchId)));

        Address address = modelMapper.map(request, Address.class);
        branch.setAddress(address);
        addressRepository.save(address);
        branchRepository.save(branch);
        return modelMapper.map(address, AddressResponse.class);

    }

    public AddressResponse update(Long branchId, Long addressId, AddressRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Branch with id %s not found", branchId)));
        Address address =addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Address with id %s not found", addressId)));
        address.setName(request.getName());
        branchRepository.save(branch);
        return modelMapper.map(address,AddressResponse.class);

    }

    public void delete(Long branchId,Long addressId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Branch with id %s not found", branchId)));
        Address address=addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Address with id %s not found", addressId)));
        addressRepository.deleteById(addressId);

    }

    public AddressResponse get(Long branchId, Long addressId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Branch with id %s not found", branchId)));
        Address address=addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException
                        (String.format("Address with id %s not found", addressId)));

        return modelMapper.map(address,AddressResponse.class);

    }

}
