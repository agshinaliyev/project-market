package az.ingress.ms14.service;

import az.ingress.ms14.dto.MarketRequest;
import az.ingress.ms14.dto.MarketResponse;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.model.Region;
import az.ingress.ms14.repository.MarketRepository;
import az.ingress.ms14.repository.RegionRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarketService {

    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;
    private final RegionRepository regionRepository;


    public MarketResponse create(MarketRequest request) {

        Market market = modelMapper.map(request, Market.class);
        marketRepository.save(market);

        return modelMapper.map(market, MarketResponse.class);

    }


    @Transactional
    public MarketResponse get(Long id) {
        Market market = marketRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", id)));

        return modelMapper.map(market, MarketResponse.class);

    }

    public void delete(Long marketId) {

        marketRepository.deleteById(marketId);
    }


    public void update(Long marketId, MarketRequest request) {

        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("market with id %s not found ", marketId)));

        market.setName(request.getName());
        market.setType(request.getType());
        marketRepository.save(market);


    }

    public Market setRegionToMarket(Long marketId, Long regionId) {

        Market market = marketRepository.findById(marketId).get();
        Region region = regionRepository.findById(regionId).get();
        market.getRegions().add(region);
        return marketRepository.save(market);


    }
}
