package az.ingress.ms14.dto;

import az.ingress.ms14.model.Branch;
import az.ingress.ms14.model.Region;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketResponse {
    Long id;


    String name;
    String type;
    List<BranchResponse> branches = new ArrayList<>();
    List<RegionResponse> regions =new ArrayList<>();

}
