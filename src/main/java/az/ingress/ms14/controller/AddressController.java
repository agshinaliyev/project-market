package az.ingress.ms14.controller;


import az.ingress.ms14.dto.AddressRequest;
import az.ingress.ms14.dto.AddressResponse;
import az.ingress.ms14.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @PostMapping("/{branchId}")
    public AddressResponse create(@PathVariable Long branchId,
                                  @RequestBody AddressRequest request ){

        return   addressService.create(branchId, request);
    }

    @PutMapping("/{branchId}/address/{addressId}")
    public AddressResponse update(@PathVariable Long branchId,@PathVariable Long addressId, @RequestBody AddressRequest request) {
        return addressService.update(branchId,addressId, request);
    }
    @DeleteMapping("/{branchId}/address/{addressId}")
    public void delete(@PathVariable Long branchId,@PathVariable Long addressId){
        addressService.delete(branchId,addressId);
    }
    @GetMapping("/{branchId}/address/{addressId}")
    public AddressResponse get(@PathVariable Long branchId,@PathVariable Long addressId){
        return addressService.get(branchId,addressId);

    }


}
