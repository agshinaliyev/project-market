package az.ingress.ms14.controller;


import az.ingress.ms14.dto.RegionRequest;
import az.ingress.ms14.dto.RegionResponse;
import az.ingress.ms14.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/region")
@RequiredArgsConstructor
public class RegionController {
    private final RegionService regionService;


    @PostMapping
    public RegionResponse create(@RequestBody RegionRequest request){

        return  regionService.create(request);

    }
    @PutMapping("/{regionId}")
    public void update(@PathVariable Long regionId,
                       @RequestBody RegionRequest request){

        regionService.update(regionId,request);


    }
    @GetMapping("/{regionId}")
    public RegionResponse get(@PathVariable Long regionId){
        return regionService.get(regionId);

    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        regionService.delete(id);
    }
}
