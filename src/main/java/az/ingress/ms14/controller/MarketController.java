package az.ingress.ms14.controller;


import az.ingress.ms14.dto.MarketRequest;
import az.ingress.ms14.dto.MarketResponse;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {
    private final MarketService marketService;

    @PostMapping
    public MarketResponse create(@RequestBody MarketRequest request) {

        return marketService.create(request);
    }

    @GetMapping("/{marketId}")
    public MarketResponse get(@PathVariable Long marketId) {

        return marketService.get(marketId);
    }

    @DeleteMapping("/{marketId}")
    public void delete(@PathVariable Long marketId) {

        marketService.delete(marketId);
    }

    @PutMapping("/{marketId}")
    public void update(@PathVariable Long marketId, @RequestBody MarketRequest request) {

        marketService.update(marketId, request);


    }

    @PutMapping("/{marketId}/region/{regionId}")
    public Market setRegionToMarket(@PathVariable Long marketId,
                                       @PathVariable Long regionId){
        return marketService.setRegionToMarket(marketId,regionId);
    }


}
