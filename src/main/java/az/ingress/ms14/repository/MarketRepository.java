package az.ingress.ms14.repository;

import az.ingress.ms14.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarketRepository extends JpaRepository<Market, Long> {
}
