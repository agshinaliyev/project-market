package az.ingress.ms14.repository;

import az.ingress.ms14.model.Address;
import az.ingress.ms14.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
