package az.ingress.ms14.repository;

import az.ingress.ms14.model.Branch;
import az.ingress.ms14.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BranchRepository extends JpaRepository<Branch, Long> {

    @Query(value = "select b from Branch b where b.market.id = :id ")
    List<Branch> getFromDb(Long id);


}
