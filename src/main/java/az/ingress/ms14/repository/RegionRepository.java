package az.ingress.ms14.repository;

import az.ingress.ms14.model.Region;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionRepository extends JpaRepository<Region,Long> {
}
